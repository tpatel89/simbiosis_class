package android.trulia.com.helloworldapp;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by tpatel on 4/21/15.
 */
public class Person implements Parcelable {

    private String name;
    private String city;

    public Person(String mName, String mCity){
        name = mName;
        city = mCity;
    }
    public Person(Parcel src){
        this.name = src.readString();
        this.city = src.readString();
    }

    private Creator<Person> CREATOR = new Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel source) {
            return new Person(source);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[0];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(city);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
