package android.trulia.com.helloworldapp.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.trulia.com.helloworldapp.InternetThread;
import android.trulia.com.helloworldapp.Person;
import android.trulia.com.helloworldapp.R;
import android.trulia.com.helloworldapp.broadcasts.MyReceiver;
import android.trulia.com.helloworldapp.db.HelloDbHelper;
import android.trulia.com.helloworldapp.fragments.ItemFragment;
import android.trulia.com.helloworldapp.fragments.NewFragment;
import android.trulia.com.helloworldapp.service.MyService;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class MainActivity extends ActionBarActivity implements View.OnClickListener,
        NewFragment.OnFragmentInteractionListener,
        ItemFragment.OnFragmentInteractionListener,
        InternetThread.listeners {

    private SQLiteDatabase helloDb;
    private HelloDbHelper dbHelper;

    private TextView dataTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main);
        if(savedInstanceState != null && savedInstanceState.containsKey("SCREEN_ROTATE")){
            Toast.makeText(this, savedInstanceState.getString("SCREEN_ROTATE"), Toast.LENGTH_LONG).show();
        }
        Log.e("MyApp", "On create is created");
        NewFragment newFrag = NewFragment.newInstance();
//        FrameLayout fmLayout = (FrameLayout)findViewById(R.id.fragment_container);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.fragment_container, newFrag, "BLANK_FRAG");
        ft.addToBackStack("BLANK_FRAG");
        ft.commit();

        dbHelper = new HelloDbHelper(this);
        helloDb = dbHelper.getWritableDatabase();

        Intent i = new Intent(this, MyReceiver.class);
        sendBroadcast(i);

        final Intent is = new Intent(this, MyService.class);
        bindService(is, new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                MyService.MyServiceBinder binder = (MyService.MyServiceBinder) service;
                binder.MessageFromActivity("Hello to Service From Act");
                unbindService(this);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                Log.e("Service Error", "Service didn't bind");
            }
        }, Service.BIND_AUTO_CREATE);
        dataTv = (TextView)findViewById(R.id.url_data);
        new Downloader().execute("", "");
    }

    @Override
    public void iwantsomething() {

    }

    @Override
    public void onDataDownlaoded(String data) {

    }

    public void iWantNewFragmentCallback(){

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("SCREEN_ROTATE", "Hello from rotation");
    }

    @Override
    public void onClick(View v) {
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.e("MyApp", "On start is created");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("MyApp", "On resume is created");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("MyApp", "On pause is created");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("MyApp", "On stop is created");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "On destroy is called", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

        Intent i = new Intent(this, MainActivity2Activity.class);
        Bundle newBundle = new Bundle();
        newBundle.putParcelable("key", new Person("Tapan", "SF"));
        newBundle.putString("to_another_activity", "Hello from Act 1");
        i.putExtra("big_bundle", newBundle);

        SharedPreferences prefs = getApplicationContext().getSharedPreferences("global", MODE_PRIVATE);
        prefs.edit().putString("new_val", "Hello from shared prefs").commit();
        startActivity(i);
//        Toast.makeText(this, "Click Handled from activity", Toast.LENGTH_LONG).show();
//        ItemFragment itemFrag = ItemFragment.newInstance();
//        getFragmentManager().beginTransaction().replace(R.id.fragment_container, itemFrag, "ITEM_FRAG").commit();
    }

    @Override
    public void onFragmentInteraction(String id) {

    }

    /**
     * Created by tpatel on 4/23/15.
     */
    public class Downloader extends AsyncTask<String, Void, String> {

        String StringUrl;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            StringUrl = "http://www.yahoo.com";
        }

        @Override
        protected void onPostExecute(String statusCode) {
            super.onPostExecute(statusCode);
            Log.d("AsyncTask", statusCode);
        }

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            //Create connection
            URL url = null;
            String urlParameters = "";
            try {
                url = new URL(StringUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(true);

                //Send request
                DataOutputStream wr = new DataOutputStream(
                        connection.getOutputStream());
                wr.writeBytes(urlParameters);
                wr.flush();
                wr.close();

                //Get Response
                InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
                rd.close();
                dataTv.setText(response.toString());
                return response.toString();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}
