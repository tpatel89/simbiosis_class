package android.trulia.com.helloworldapp.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import java.io.FileDescriptor;

public class MyService extends Service {
    public MyService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("Service", "My service started");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public void onDestroy() {
        Log.e("Service", "My Service destroyed");
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new MyServiceBinder(this);
    }

    public class MyServiceBinder extends Binder{

        public MyServiceBinder(Context act){

        }

        public void MessageFromActivity(String message){
            Toast.makeText(MyService.this, message, Toast.LENGTH_SHORT).show();
        }
    }
}
