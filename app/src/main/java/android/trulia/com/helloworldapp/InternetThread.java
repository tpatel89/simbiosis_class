package android.trulia.com.helloworldapp;

import android.app.Activity;

import java.util.ArrayList;

/**
 * Created by tpatel on 4/23/15.
 */
public class InternetThread extends Thread {

    private Runner executor;
    private ArrayList<listeners> list;

    public InternetThread(){
        super();
        list = new ArrayList<>();

    }

    public void setListener(listeners act){
        list.add(act);
    }

    private class Runner implements Runnable{
        @Override
        public void run() {
            //Download all the data;
            for(listeners item: list){
                item.onDataDownlaoded("");
            }
        }
    }

    public interface listeners{
        void onDataDownlaoded(String data);
    }

}
