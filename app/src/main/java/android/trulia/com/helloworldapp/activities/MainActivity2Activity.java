package android.trulia.com.helloworldapp.activities;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.trulia.com.helloworldapp.R;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


public class MainActivity2Activity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity2);
//        String var = getIntent().getExtras().getBundle("big_bundle").getString("to_another_activity");
        String var = getApplicationContext().getSharedPreferences("global", MODE_PRIVATE).getString("new_val", "def");
        Toast.makeText(this, var, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
