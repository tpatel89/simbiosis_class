import android.test.AndroidTestCase;
import android.trulia.com.helloworldapp.Summer;

/**
 * Created by tpatel on 4/23/15.
 */
public class SummerTest extends AndroidTestCase {

    public SummerTest() {
        super();
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testSummer(){
        Summer sum = new Summer();
        int ans = sum.addition(2, 2);
        assertEquals(5, ans);
        int div = sum.division(10,2);
        assertEquals(5, div);
    }


}
